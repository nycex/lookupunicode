# lookupunicode
swizz army knife for unicode - a utility to search in the unicode database

## dependencies
- https://www.unicode.org/Public/13.0.0/ucd/UnicodeData.txt (substitute 13.0.0 with the most recent unicode version) (Arch Linux Package: [unicode-character-database](https://www.archlinux.org/packages/extra/any/unicode-character-database/))

## build
- specify data path with `DATA_PATH` env variable (default is "/usr/share/unicode/UnicodeData.txt")
- run `cargo build --release`

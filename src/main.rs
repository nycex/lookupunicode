mod char;
use crate::char::Char;
use anyhow::Result;
use indoc::printdoc;
use regex::Regex;

#[derive(PartialEq)]
enum Mode {
    Mixed,
    Search,
    Hexadecimal,
    Character,
}

// defines const DATA: [Char; _]
include!(concat!(env!("OUT_DIR"), "/data.rs"));

fn main() -> Result<()> {
    let args: Vec<_> = std::env::args().collect();

    debug_assert!(!args.is_empty());

    if args.len() == 1 {
        print_usage(&args[0], 1);
    }

    let mut start_index = 1;

    let mode: Mode = {
        if args[1].starts_with('-') {
            start_index += 1;
            if args.len() < 3 {
                let exit_code = if args[1] == "-h" || args[1] == "--help" {
                    0
                } else {
                    1
                };
                print_usage(&args[0], exit_code);
            }

            match args[1].as_str() {
                "-s" | "--search" => Mode::Search,
                "-x" | "--hex" => Mode::Hexadecimal,
                "-c" | "--character" => Mode::Character,
                _ => print_usage(&args[0], 1),
            }
        } else {
            Mode::Mixed
        }
    };

    let query = &(args.as_slice())[start_index..args.len()];

    let regexs = if mode == Mode::Mixed || mode == Mode::Hexadecimal || mode == Mode::Search {
        let mut vec = Vec::new();
        for word in query {
            let upper = word.to_ascii_uppercase();
            vec.push(Regex::new(&upper)?);
        }
        Some(vec)
    } else {
        None
    };

    let chars = if mode == Mode::Mixed || mode == Mode::Character {
        let mut vec = Vec::new();
        for word in query {
            for ch in word.chars() {
                vec.push(ch);
            }
        }
        Some(vec)
    } else {
        None
    };

    let mut chars_printed = false;

    let mut mixed_char_chars = if mode == Mode::Mixed {
        let vec: Vec<Char> = Vec::new();
        Some(vec)
    } else {
        None
    };

    let mixed = mode == Mode::Mixed;

    for character in DATA.iter() {
        let mut should_print = false;

        if !should_print && (mode == Mode::Search || mixed) {
            should_print = true;
            for regex in (&regexs).as_ref().unwrap() {
                if !regex.is_match(character.name) {
                    should_print = false;
                    break;
                }
            }
            if should_print {
                chars_printed = true;
            }
        }

        if !should_print && (mode == Mode::Hexadecimal || mixed) {
            should_print = true;
            for regex in (&regexs).as_ref().unwrap() {
                if !regex.is_match(character.codepoint) {
                    should_print = false;
                    break;
                }
            }
            if should_print {
                chars_printed = true;
            }
        }

        if !should_print && (mode == Mode::Character || mixed) {
            for ch in (&chars).as_ref().unwrap() {
                if *ch == character.scalar {
                    if mixed {
                        mixed_char_chars
                            .as_mut()
                            .unwrap()
                            .push((*character).clone());
                    } else {
                        should_print = true;
                    }
                    break;
                }
            }
        }

        if should_print {
            print_char(character);
        }
    }
    if mode == Mode::Mixed && !chars_printed {
        for character in mixed_char_chars.unwrap() {
            print_char(&character);
        }
    }
    Ok(())
}

fn print_char(character: &Char) {
    println!(
        "'{}'\t{}\t{}",
        character.scalar, character.codepoint, character.name
    );
}

fn print_usage(executable: &str, exit_code: i32) -> ! {
    printdoc!(
        r#"
        Usage: {} [mode] <query>
        search in the unicode database
        Example: {} -x "^000A$"

        Modes (search by):
          -s, --search    name of the character
          -x, --hex       ascii hexform of the character's codepoint
          -c, --character actual encoded character

        Other:
          -h, --help      show this help page
        "#,
        executable,
        executable
    );
    std::process::exit(exit_code);
}
